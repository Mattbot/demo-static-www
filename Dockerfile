FROM quay.io/mattbot/base:ubuntu-18.04-1

# Tell Apt never to prompt
ENV DEBIAN_FRONTEND noninteractive

RUN : \
  && apt-get -y update \
  && apt-get install -y --no-install-recommends \
    hugo \
    moreutils\
  && rm -rf /var/lib/app/lists/*

WORKDIR /app

COPY ./archetypes /app/archetypes
COPY ./content /app/content
COPY ./data /app/data
COPY ./docker /app/docker
COPY ./layouts /app/layouts
COPY ./static /app/static
COPY ./themes /app/themes
COPY ./config.toml /app/config.toml

ENV HUGO_BASE_URL ''
ENV HUGO_APPEND_PORT 'true'
ENV HUGO_PORT 34255

# RUN (echo "baseurl = '${HUGO_BASE_URL}'"; cat /app/config.toml) | sponge /app/config.toml
# RUN head -2 /app/config.toml

# ENTRYPOINT ["/sbin/tini", "-vvg", "--", "/app/docker/entrypoint.sh"]

STOPSIGNAL SIGINT

EXPOSE 34255

RUN hugo

CMD hugo server -t hugo-creative-theme --appendPort=${HUGO_APPEND_PORT} --bind=0.0.0.0 -p ${HUGO_PORT} -b ${HUGO_BASE_URL}

# CMD [ "/usr/bin/hugo", "server", "-p", "3000", "-t", "hugo-creative-theme", "--bind=0.0.0.0" ]
